#include <stdio.h>

float x;
int   i;

int main(void) {

	i = 42;
	x = 3.10;

	printf("i=%d\n", i);
	printf("x=%.2f\n", x);
	
	return 0;
}
